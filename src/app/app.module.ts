import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule} from '@angular/fire';
import { AngularFireAuthModule} from '@angular/fire/auth';
import { CommonModule } from '@angular/common';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './componente/login/login.component';
import { RegistroComponent } from './componente/registro/registro.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PrincipalComponent } from './componente/principal/principal.component';
import { ProductoComponent } from './componente/producto/producto.component';
import { ProductoInfoComponent } from './componente/producto-info/producto-info.component';
import { ProductosListadoComponent } from './componente/productos-listado/productos-listado.component';
import { UsuarioInfoComponent } from './componente/usuario-info/usuario-info.component';
import { UsuariosListadoComponent } from './componente/usuarios-listado/usuarios-listado.component';
import { TableFilterPipe } from './table-filter.pipe';
import { LocalesListadoComponent } from './componente/locales-listado/locales-listado.component';
import { LocalInfoComponent } from './componente/local-info/local-info.component';
import { ClienteAbmComponent } from './componente/cliente-abm/cliente-abm.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistroComponent,
    PrincipalComponent,
    ProductoComponent,
    ProductoInfoComponent,
    ProductosListadoComponent,
    UsuarioInfoComponent,
    UsuariosListadoComponent,
    TableFilterPipe,
    LocalesListadoComponent,
    LocalInfoComponent,
    ClienteAbmComponent,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),  
    AngularFireAuthModule,
    AngularFirestoreModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDropzoneModule,
    BrowserModule, 
    RouterModule,
   
  ],
  providers: [ AngularFireStorage],
  bootstrap: [AppComponent]
})
export class AppModule { }
