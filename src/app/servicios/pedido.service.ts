import { Injectable, Input, Output, EventEmitter } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { map, take, delay, tap } from 'rxjs/operators'
import { Observable, of } from 'rxjs';
import { Usuario } from '../clases/usuario';
import { AngularFirestore } from '@angular/fire/firestore';
import { Producto } from '../clases/producto';
import { Pedido } from '../clases/pedido';
import { AuthService } from './auth.service';

import * as randomID from 'randomatic';

@Injectable({
  providedIn: 'root'
})
export class PedidoService {

  porcentaje: number;
  finalizado: boolean;
  productoSelecinado: any;
  isLoggedIn: boolean;
  email: string = "no de";
  sucursal: string = "no def";
  localSelecinado: any;

  constructor(
    private storagefoto: AngularFireStorage,
    private fireStore: AngularFirestore,
    private authservice: AuthService
  ) {
    this.email = this.authservice.usuarioLoguiado.email;
    //this.sucursal = this.authservice.usuarioLoguiado.sucursal;
    console.info(this.email);

  }


  buscarUsuarioPorEmail(codigo: string) {
    let productos = this.fireStore.collection('productos', ref => ref.where('codigo', '==', codigo)).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return productos;
  }

  documentToDomainObject = _ => {
    const object = _.payload.doc.data();
    object.id = _.payload.doc.id;
    object.codigo = _.payload.doc.id;
    return object;
  }


  actualizarDatos(id: any, res: any) {
    this.fireStore.collection('productos').doc(id).collection('provedoresAuth').add({ res });
  }

  //var file = campoArchivo.get(0).files[0];
  async subir(pedido: Pedido, filename?: string, file?: any) {
    let codigo = randomID('Aa0!', 5);
    // Admin SDK only
    this.traerTodos('pedidos').subscribe(res => console.info(res)

    );
    let fecha = new Date();
    if (file != undefined && filename != undefined) {
      var ref = this.storagefoto.ref('pedidos' + '/' + pedido.codigo + '_' + filename).put(file);
      ref.percentageChanges().subscribe((porcentaje) => {
        this.porcentaje = Math.round(porcentaje);
        console.log("Porcentaje:" + this.porcentaje)
        if (this.porcentaje == 100) {
          this.finalizado = true;
          setTimeout(() => this.storagefoto.ref('pedidos' + '/' + pedido.codigo + '_' + filename).getDownloadURL().subscribe((URL) => {
            console.log(URL);
            pedido.foto = URL;
            var res = this.fireStore.collection('pedidos').add(JSON.parse(JSON.stringify(pedido))).then((docRef) => {
              // Update metadata properties

              console.info(docRef);

              let dataFoto = {
                contentType: 'image/jpeg',
                customMetadata: {
                  'location': 'Buenos Aires',
                  'activity': 'text',
                  'usuario': docRef.id
                }
              };


              return dataFoto;
            });
            res.then(meta => {
              this.storagefoto.ref('pedidos' + '/' + pedido.codigo + '_' + filename).updateMetadata(meta);
              console.log('meta ok');

            })
              .catch(function (error) {
                // Uh-oh, an error occurred!
                alert('error meta');
              });

          }), 3000);
        }
      });
    } else {
      var res = this.fireStore.collection('pedidos').doc(codigo).set(JSON.parse(JSON.stringify(pedido)))
        .then(function () {
          console.log("Document successfully written!");
        })
        .catch(function (error) {
          console.error("Error writing document: ", error);
        });
    }

  }


  traerTodos(coleccion: string) {
    let usuarios = this.fireStore.collection('/' + coleccion).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;

  }


  traerTodosMovimientos(uid: string, coleccion: string) {
    let usuarios = this.fireStore.collection(coleccion).doc(uid).collection('/movimientos', ref => ref.orderBy('fecha', 'desc')).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;

  }


  buscarUId(uid: string) {
    let ref = this.fireStore.collection('productos').doc(uid);
    let getDoc = ref.get();
    console.info(getDoc);
    return getDoc;
  }

  verDetalle(producto: any): Observable<boolean> {
    this.productoSelecinado = producto;
    return of(true).pipe(
      delay(1000),
      tap(val => this.isLoggedIn = true)
    );
  }

  verDetalleLocal(producto: any): Observable<boolean> {
    this.localSelecinado = producto;
    console.info(this.localSelecinado);
    return of(true).pipe(
      delay(1000),
      tap(val => this.isLoggedIn = true)
    );
  }


  cambiarStock(producto: Producto, cantidad: number) {
    let ref = this.fireStore.collection('productos').doc(producto.codigo);
    let email = this.email;
    let sucursal = this.sucursal;
    let fecha = new Date();
    ref.update({
      cantidad: cantidad,
    });

    const log = {
      usuario: email,
      fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
      local: sucursal,
      operacion: 'cambio Stock',
      stock: cantidad
    };

    let movimientos = ref.collection('movimientos').add(log);

    const dataLocal = {
      usuario: email,
      nombre: producto.nombre,
      fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
      operacion: 'cambio Stock',
      stock: cantidad
    };

    let movimientosLocal = this.addLocalMovimientos(sucursal, dataLocal).then(() => {
    });
  }

  prueba() {
    return this.fireStore.collection('locales').doc('boca').collection('movimientos').add({
      name: 'Golden Gate Bridge',
      type: 'bridge'
    });
  }


  addLocalMovimientos(local: string, data: any) {
    return this.fireStore.collection('locales').doc(local).collection('movimientos').add(data);
  }




}