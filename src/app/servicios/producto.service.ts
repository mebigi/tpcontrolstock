import { Injectable, Input, Output, EventEmitter } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';

import { map, take, delay, tap } from 'rxjs/operators'

import { Observable, of } from 'rxjs';
import { Usuario } from '../clases/usuario';
import { AngularFirestore } from '@angular/fire/firestore';
import { Producto } from '../clases/producto';
import { AuthService } from './auth.service';
import { dependenciesFromGlobalMetadata } from '@angular/compiler/src/render3/r3_factory';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  porcentaje: number;
  finalizado: boolean;
  productoSelecinado: any;
  isLoggedIn: boolean;
  email: string = "no de";
  sucursal: string = "no def";
  localSelecinado: any;

  constructor(
    private storagefoto: AngularFireStorage,
    private fireStore: AngularFirestore,
    private authservice: AuthService
  ) {
    this.email = this.authservice.usuarioLoguiado.email;
    //this.sucursal = this.authservice.usuarioLoguiado.sucursal;
    console.info(this.email);

  }


  buscarUsuarioPorEmail(codigo: string) {
    let productos = this.fireStore.collection('productos', ref => ref.where('codigo', '==', codigo)).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return productos;
  }

  documentToDomainObject = _ => {
    const object = _.payload.doc.data();
    object.id = _.payload.doc.id;
    object.codigo = _.payload.doc.id;
    return object;
  }


  actualizarDatos(id: any, res: any) {
    this.fireStore.collection('productos').doc(id).collection('provedoresAuth').add({ res });
  }

  //var file = campoArchivo.get(0).files[0];
  async subir(filename: string, file: any, producto: Producto) {
    let email = this.email;
    let sucursal = this.sucursal;
    let fecha = new Date();
    var ref = this.storagefoto.ref('productos' + '/' + producto.nombre + '_' + filename).put(file);
    ref.percentageChanges().subscribe((porcentaje) => {
      this.porcentaje = Math.round(porcentaje);
      console.log("Porcentaje:" + this.porcentaje)
      if (this.porcentaje == 100) {
        this.finalizado = true;
        setTimeout(() => this.storagefoto.ref('productos' + '/' + producto.nombre + '_' + filename).getDownloadURL().subscribe((URL) => {
          console.log(URL);
          const data = {
            nombre: producto.nombre,
            costo: producto.costo,
            cantidad: 0,
            fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
            URL,
            descripcion: producto.descripcion,
            categoria: producto.categoria,           
          }



          var res = this.fireStore.collection('productos').add(data).then( (docRef) => {
            // Update metadata properties

            const log = {
              usuario: email,
              fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
              local: sucursal,
              operacion: 'creado',
              stcok: 0
            };

            console.info(docRef);

            let dataFoto = {
              contentType: 'image/jpeg',
              customMetadata: {
                'location': 'Buenos Aires',
                'activity': 'text',
                'usuario': docRef.id
              }
            };

            let movimientos = docRef.collection('movimientos').add(log).then(() => {

              const dataLocal = {
                usuario: email,
                nombre: producto.nombre,
                fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
                operacion: 'creado',
                stock: 0
              };

              let movimientosLocal = this.addLocalMovimientos(sucursal, dataLocal).then(() => {
            
              });
            //this.fireStore.doc(docRef.path).Update.add(data);
          });
          return dataFoto;
        });
        res.then(meta => {

          this.storagefoto.ref('productos' + '/' + producto.nombre + '_' + filename).updateMetadata(meta);
          console.log('meta ok');

        })
          .catch(function (error) {
            // Uh-oh, an error occurred!
            alert('error meta');
          });

      }), 3000);
  }
});

  }


traerTodos(coleccion:string) {
  let usuarios = this.fireStore.collection('/'+coleccion).snapshotChanges()
    .pipe(map(actions => actions.map(this.documentToDomainObject)));
  return usuarios;

}


traerTodosMovimientos(uid: string, coleccion:string) {
  let usuarios = this.fireStore.collection(coleccion).doc(uid).collection('/movimientos', ref => ref.orderBy('fecha', 'desc')).snapshotChanges()
    .pipe(map(actions => actions.map(this.documentToDomainObject)));
  return usuarios;

}


buscarUId(uid: string) {
  let ref = this.fireStore.collection('productos').doc(uid);
  let getDoc = ref.get();
  console.info(getDoc);
  return getDoc;
}

verDetalle(producto: any): Observable < boolean > {
  this.productoSelecinado = producto;
  return of(true).pipe(
    delay(1000),
    tap(val => this.isLoggedIn = true)
  );
}

verDetalleLocal(producto: any): Observable < boolean > {
  this.localSelecinado = producto;
  console.info( this.localSelecinado);
  return of(true).pipe(
    delay(1000),
    tap(val => this.isLoggedIn = true)
  );
}


cambiarStock(producto: Producto, cantidad: number) {
  let ref = this.fireStore.collection('productos').doc(producto.codigo);
  let email = this.email;
  let sucursal = this.sucursal;
  let fecha = new Date();
  ref.update({
    cantidad: cantidad,
  });

  const log = {
    usuario: email,
    fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
    local: sucursal,
    operacion: 'cambio Stock',
    stock: cantidad
  };

  let movimientos = ref.collection('movimientos').add(log);

  const dataLocal = {
    usuario: email,
    nombre: producto.nombre,
    fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
    operacion: 'cambio Stock',
    stock: cantidad
  };

  let movimientosLocal = this.addLocalMovimientos(sucursal, dataLocal).then(() => {            
  });
}

prueba() {
  return this.fireStore.collection('locales').doc('boca').collection('movimientos').add({
    name: 'Golden Gate Bridge',
    type: 'bridge'
  });
}


addLocalMovimientos(local: string, data: any) {
  return this.fireStore.collection('locales').doc(local).collection('movimientos').add(data);
}




}