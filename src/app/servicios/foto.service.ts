import { Injectable, Input, Output, EventEmitter } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';

import { map, take, delay, tap } from 'rxjs/operators'

import { Observable, of } from 'rxjs';
import { Usuario } from '../clases/usuario';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FotoService {
  porcentaje: number;
  finalizado: boolean;
  productoSelecinado: any;

  usuarioSelecinado: any;
  isLoggedIn: boolean;


  constructor(
    private storagefoto: AngularFireStorage,
    private fireStore: AngularFirestore,
  ) {

  }

  buscarUsuarioPorEmail(email:string) {   
    let usuarios = this.fireStore.collection('usuarios', ref => ref.where('email', '==', email)).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;
  }


  buscarUsuarioPorSucursal(local:string) {   
    let usuarios = this.fireStore.collection('usuarios', ref => ref.where('sucursal', '==', local)).snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;
  }

  documentToDomainObject = _ => {
    const object = _.payload.doc.data();
    object.id = _.payload.doc.id;   
    return object;
  }


  actualizarDatos(id:any, res:any){
    this.fireStore.collection('usuarios').doc(id).collection('provedoresAuth').add({ res });      
  }

  //var file = campoArchivo.get(0).files[0];
 async subir(filename: string, file: any,  usuario: Usuario) {
  let date = new Date(); 
  let fecha =  date.toLocaleDateString() + " " + date.toLocaleTimeString();
    var ref = this.storagefoto.ref(usuario.perfil + '/' + usuario.email + '_' + filename).put(file);
    ref.percentageChanges().subscribe((porcentaje) => {
      this.porcentaje = Math.round(porcentaje);
      console.log("Porcentaje:" + this.porcentaje)
      if (this.porcentaje == 100) {
        this.finalizado = true;
        setTimeout(() => this.storagefoto.ref(usuario.perfil + '/' + usuario.email + '_' + filename).getDownloadURL().subscribe((URL) => {
          console.log(URL);
          var res=this.fireStore.collection('usuarios').add({ nombre: usuario.nombre, email: usuario.email, foto: URL, perfil: usuario.perfil, sucursal: usuario.sucursal }).then(function (docRef) {
            
            const log = {
              usuario: usuario.email,
              fecha: fecha,
              local: usuario.sucursal,
              perfil: usuario.perfil,
              operacion: 'creado',
            };

            console.info(docRef);



            // Update the timestamp field with the value from the server
            /*let updateTimestamp = docRef.update({
              timestamp: 'bbb'
            });*/

            let movimientos = docRef.collection('movimientos').add(log);

            
            // Update metadata properties
            return  {
              contentType: 'image/jpeg',
              customMetadata: {
                'location': 'Buenos Aires',
                'activity': 'text',
                'usuario': docRef.id,
              }
            }
          });


          
          res.then(meta => {
              this.storagefoto.ref(usuario.perfil + '/' + usuario.email + '_' + filename).updateMetadata(meta);
              console.log('meta ok')
             })
              .catch(function (error) {
              // Uh-oh, an error occurred!
              alert('error meta');
            });
 
         

        
        }), 3000);
      }
    });

  }

  
  traerTodos() {
    let usuarios = this.fireStore.collection('usuarios').snapshotChanges()
      .pipe(map(actions => actions.map(this.documentToDomainObject)));
    return usuarios;

  }


  buscarUId(uid: string) {
    let ref = this.fireStore.collection('usuarios').doc(uid);
    let getDoc = ref.get();
    return getDoc;
  }

  verDetalle(usuario: any): Observable<boolean> {
    this.usuarioSelecinado = usuario;
    return of(true).pipe(
      delay(1000),
      tap(val => this.isLoggedIn = true)
    );
  }

 



traerTodosMovimientos(uid: string, coleccion:string) {
  let usuarios = this.fireStore.collection(coleccion).doc(uid).collection('/movimientos', ref => ref.orderBy('fecha', 'desc')).snapshotChanges()
    .pipe(map(actions => actions.map(this.documentToDomainObject)));
  return usuarios;

}





cambiar(uid: string, usuario: Usuario) {
  let ref = this.fireStore.collection('usuarios').doc(uid);
  let fecha = new Date();
  
  ref.update({
    sucursal: usuario.sucursal,
    perfil: usuario.perfil,
  });

  const log = {
    usuario: usuario.email,
    fecha: fecha.toLocaleDateString() + " " + fecha.toLocaleTimeString(),
    local: usuario.sucursal,
    perfil: usuario.perfil,
    operacion: 'modificacion'
  };
  let movimientos = ref.collection('movimientos').add(log);
}





}