import { Injectable } from '@angular/core';
import { Usuario } from '../clases/usuario';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { auth } from 'firebase';
import swal from 'sweetalert';
import * as firebase from 'firebase';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public estaloguiado: any = false;
  public usuarioLoguiado: Usuario;
  usuarios: any[];
  user:any;
  token:any;

  constructor(private angularFireAuth: AngularFireAuth, private db: AngularFirestore, private route: Router) {
    angularFireAuth.authState.subscribe(Usuario => (this.estaloguiado = Usuario));
  }


  getCurrentUser() {
    var user = this.angularFireAuth.auth.currentUser;
    var name, email, photoUrl, uid, emailVerified, token;

    if (user != null) {
      name = user.displayName;
      email = user.email;
      photoUrl = user.photoURL;
      emailVerified = user.emailVerified;
      uid = user.uid;
      token = user.getIdToken();
      console.log(token);

      // The user's ID, unique to the Firebase project. Do NOT use
      // this value to authenticate with your backend server, if
      // you have one. Use User.getToken() instead.
    }

  }

 

  login(usuario: Usuario): Promise<any> {
      return this.angularFireAuth.auth.signInWithEmailAndPassword(
      usuario.email, usuario.clave);
  }
  getCurrentUserId(): string {
    return this.angularFireAuth.auth.currentUser ? this.angularFireAuth.auth.currentUser.uid : null;
  }

  getCurrentUserMail(): string {
    return this.angularFireAuth.auth.currentUser.email;
  }

  // Sign up with email/password
  async SignUp(usuario: Usuario) {
    return this.angularFireAuth.auth.createUserWithEmailAndPassword(usuario.email, usuario.clave)
      .then((result) => {
        console.log("Se registró con éxito");
        this.route.navigate(['login']);
        console.log(result.user)
      }).catch((error) => {
        console.log(error.message)
      })
  }


 
  SignOut() {
    this.angularFireAuth.auth.signOut().then(function () {
      // Sign-out successful.
    }).catch(function (error) {
      // An error happened.
    });
  }

  facebookLogin(){
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.FacebookAuthProvider();
      this.angularFireAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      })
    })

  }

  fbRedirect(){
  
  }

 
}