export enum EstadoPedido {
    iniciado = "iniciado",
    pendiente = "pendiente",
    enpreparacion = "en Prepareción",
    finalizado = "finalizado",
    cancelado = "cancelado"
}
