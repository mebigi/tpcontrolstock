import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './componente/login/login.component';
import { RegistroComponent } from './componente/registro/registro.component';
import { ProductoComponent } from './componente/producto/producto.component';
import { ProductosListadoComponent } from './componente/productos-listado/productos-listado.component';
import { ProductoInfoComponent } from './componente/producto-info/producto-info.component';
import { UsuariosListadoComponent } from './componente/usuarios-listado/usuarios-listado.component';
import { UsuarioInfoComponent } from './componente/usuario-info/usuario-info.component';
import { PrincipalComponent } from './componente/principal/principal.component';
import { LocalInfoComponent } from './componente/local-info/local-info.component';
import { LocalesListadoComponent } from './componente/locales-listado/locales-listado.component';
import { ClienteAbmComponent } from './componente/cliente-abm/cliente-abm.component';

const routes: Routes = [

  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'Registro', component: RegistroComponent },
  { path: 'Producto', component: ProductoComponent },
  { path: 'Listado-Productos', component: ProductosListadoComponent },
  { path: 'Producto-info', component: ProductoInfoComponent },
  { path: 'Listado-Usuarios', component: UsuariosListadoComponent },
  { path: 'Usuario-info', component: UsuarioInfoComponent },
  { path: 'Local-info', component: LocalInfoComponent },
  { path: 'Locales', component: LocalesListadoComponent },
  { path: 'Principal', component: PrincipalComponent },
  { path: 'Registro/cliente', component: ClienteAbmComponent },
  
];

@NgModule({
  providers: [],
  imports: [
    RouterModule.forRoot(routes),

  ],
  exports: [RouterModule],
 
})
export class AppRoutingModule { }
