export class ProductoPedido {
    codigo: string;
    nombre:string;
    costo:number;
    cantidad: number;
    fecha:Date;
    descripcion: string;
    observaciones:string;
    URL:string;
    tipo_empledo:string;
    tiempo_estimado:number;
    estado: any; //enum
    }
    
