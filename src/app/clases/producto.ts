export class Producto {
    codigo: string;
    nombre:string;
    costo:number;
    descripcion:string;
    URL:string;
    tipo_empledo:string;
    categoria:string;
    cantidad:number;
}
