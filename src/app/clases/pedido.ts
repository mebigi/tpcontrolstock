import { Producto } from './producto';
import { Usuario } from './usuario';
import { EstadoPedido } from '../enumerados/estado-pedido.enum';

export class Pedido {
    codigo: string;
    cliente: Usuario; //clase cliente
    mesas: any[] = []; // array de mesas    
    productos:Producto[]; // array de porductos cada prodcuto con su cantidad precio
    mozo: Usuario; // empledo tipo mozo
    fecha:Date;
    estado: EstadoPedido;// (enum)
    foto:string;
    constructor(uncliente?: Usuario){
        
        if(uncliente != undefined){
            this.cliente=uncliente;
        } else{
            this.cliente= new Usuario();
        }

    }
}
