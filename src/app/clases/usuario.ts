export class Usuario 
{
	public id: string;
    public nombre: string;
    public email: string;
	public clave:string;
	public perfil:string;
	public foto:string;
	public estado:string
	public sucursal:string;


	constructor(email?: string, clave?:string)
	{
		this.email = email;
        this.clave = clave;
	}
}