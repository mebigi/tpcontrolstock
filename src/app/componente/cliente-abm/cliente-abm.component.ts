import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/servicios/auth.service';
import { Usuario } from 'src/app/clases/usuario';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { FotoService } from 'src/app/servicios/foto.service';
import { ProductoService } from 'src/app/servicios/producto.service';

@Component({
  selector: 'app-cliente-abm',
  templateUrl: './cliente-abm.component.html',
  styleUrls: ['./cliente-abm.component.css']
})
export class ClienteAbmComponent implements OnInit {
  public nuevoUsuario: Usuario;
  public archivo: File;
  files: File[] = [];
  usuario: Usuario;
  perfil: string;
  locales: any[];


  constructor(private servicioproductos: ProductoService ,private authservice: AuthService,public fotoservice: FotoService) { 
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil;
    this.nuevoUsuario = new Usuario();
  }

  ngOnInit() {
    this.servicioproductos.traerTodos('locales')
    .subscribe(productos => {
      this.locales = productos;
      // console.log(this.usuarios[0].numero);
    });
  }

 

	onSelect($event) {
		console.log($event);
		this.files.push(...$event.addedFiles);
    this.archivo = this.files[0];
	}

	onRemove($event) {
		console.log($event);
		this.files.splice(this.files.indexOf($event), 1);
  }
  
  registrarUsuario(){
    console.log("sucursal:",this.nuevoUsuario.sucursal);
    //lo agrego parar registro de login
    this.authservice.SignUp(this.nuevoUsuario);
    //lo agrego a la base y subo la fotos al storage
    this.fotoservice.subir(this.archivo.name, this.archivo, this.nuevoUsuario);
  }
  
 //var file = campoArchivo.get(0).files[0];
//subir(filename: string, file: any, perfil: string, usuario: Usuario) {}

}
