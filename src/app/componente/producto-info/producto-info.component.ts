import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/servicios/producto.service';
import { Producto } from 'src/app/clases/producto';
import { Usuario } from 'src/app/clases/usuario';
import { FotoService } from 'src/app/servicios/foto.service';
import { AuthService } from 'src/app/servicios/auth.service';

@Component({
  selector: 'app-producto-info',
  templateUrl: './producto-info.component.html',
  styleUrls: ['./producto-info.component.css']
})
export class ProductoInfoComponent implements OnInit {
  public producto: any;
  movimientos: any[];
  cantidad: number;
  usuario: Usuario;
  perfil: string;
  filtro: any;


  constructor(private serviciousuarios: FotoService, private authservice: AuthService, private servicioproductos: ProductoService) { 
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil;   
    this.producto = this.servicioproductos.productoSelecinado;
   
  }

  ngOnInit() {
    this.traerTodaMovimientos();
    this.cantidad= this.producto.cantidad;
  }

  getUnique(arr, comp) {
    var unique = arr
        .map(function (e) { return e[comp]; })
        // store the keys of the unique objects
        .map(function (e, i, final) { return final.indexOf(e) === i && i; })
        // eliminate the dead keys & store unique objects
        .filter(function (e) { return arr[e]; }).map(function (e) { return arr[e]; });
    return unique;
}



 


  traerTodaMovimientos() {
    this.servicioproductos.traerTodosMovimientos(this.producto.codigo, 'productos')
      .subscribe(movimientos => {
        this.movimientos = movimientos;
        this.filtro = this.getUnique(this.movimientos, 'nombre');
      });

  }

  cambiarStock(){
    this.movimientos=[];
    this.servicioproductos.cambiarStock(this.producto, this.cantidad);
  }

}
