
import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/servicios/producto.service';
import { Producto } from 'src/app/clases/producto';
import { Usuario } from 'src/app/clases/usuario';
import { FotoService } from 'src/app/servicios/foto.service';
import { AuthService } from 'src/app/servicios/auth.service';

@Component({
  selector: 'app-local-info',
  templateUrl: './local-info.component.html',
  styleUrls: ['./local-info.component.css']
})
export class LocalInfoComponent implements OnInit {

  public local: any;
  movimientos: any[];
  cantidad: number;
  usuario: Usuario;
  perfil: string;
  usuarios: Usuario[];
  filtro: any;


  constructor(private serviciousuarios: FotoService, private authservice: AuthService, private servicioproductos: ProductoService) { 
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil;   
    this.local = this.servicioproductos.localSelecinado;
    console.log('this.local.nombre', this.local.nombre);
   
  }

  ngOnInit() {
    this.traerTodaMovimientos();
    this.serviciousuarios.buscarUsuarioPorSucursal(this.local.nombre).subscribe(usuarios => {
      this.usuarios = usuarios;
    });
    
  }

  traerTodaMovimientos() {
    this.servicioproductos.traerTodosMovimientos(this.local.id, 'locales')
      .subscribe(movimientos => {
        this.movimientos = movimientos;
        this.filtro = this.getUnique(this.movimientos, 'nombre');
      });
  }


  getUnique(arr, comp) {
    var unique = arr
        .map(function (e) { return e[comp]; })
        // store the keys of the unique objects
        .map(function (e, i, final) { return final.indexOf(e) === i && i; })
        // eliminate the dead keys & store unique objects
        .filter(function (e) { return arr[e]; }).map(function (e) { return arr[e]; });
    return unique;
}



 

  }

  
