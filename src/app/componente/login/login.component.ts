import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/servicios/auth.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import swal from 'sweetalert';
import { Usuario } from 'src/app/clases/usuario';
import { FotoService } from 'src/app/servicios/foto.service';
import { Routes, ActivatedRoute, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  spinner = false;
  usuario: Usuario = new Usuario();
  private ok: boolean; //Login OK
  private error: boolean; //Login fallido
  public formulario: FormGroup;
  private errorDatos: boolean; //Error en el formato de datos de correo o clave
  private enEspera: boolean; //Muestra u oculta el spinner

  // constructor(public authservicio: AuthService,  public fotoservice: FotoService, private router: Router, private route: ActivatedRoute, public formBuilder: FormBuilder) { }
  constructor(private route: ActivatedRoute, private router: Router, public formBuilder: FormBuilder, public authservicio: AuthService, public fotoservice: FotoService) {
    this.formulario = this.formBuilder.group(
      {
        correo: ['', Validators.compose([Validators.email, Validators.required])],
        clave: ['', Validators.compose([Validators.minLength(6), Validators.required])]
      });

    this.loading();

  }

  ngOnInit() {
    //this.authservicio.facebookLogin

  }

  


  loading() {
    this.ok = false;
    this.error = false;
    this.errorDatos = false;
    this.enEspera = false;

    setTimeout(() => {    //<<<---    using ()=> syntax
     // this.spinner = false;
    }, 3000);

  }

  public getError(form: any, controlName: string): string {
    let error: any;
    let mse = "";
    const control = form.get(controlName);
    if (control.touched && control.errors != null) {
      console.info(JSON.stringify(control.errors));
      error = JSON.parse(JSON.stringify(control.errors));
      if (error.required) {
        mse = "Campo requerido.";
      }
      if (error.minlength != undefined) {
        mse = "Error en logintud mínima requerida.";
      }
      if (error.maxlength != undefined) {
        mse = "Error en la longitud máxima.";
      }

      if (error.pattern != undefined) {
        mse = "Error en el tipo de dato.";
      }
    }
    return mse;
  }


  /* login2() {
 
      if (this.formulario.valid) {
 
       this.usuario.email = this.formulario.value.correo;
       this.usuario.clave = this.formulario.value.clave;
 
       this.spinner = true;
       this.authservicio.usuarioLoguiado = new Usuario();
       this.authservicio.login(this.usuario).then(() => {
         setTimeout(() => {
           this.usuarioServicio.buscarUsuarioPorEmail(this.usuario.email).subscribe(usuarios => {
             this.authservicio.usuarioLoguiado = usuarios[0];
             this.spinner = false;
             //console.log(this.authservicio.usuarioLoguiado.perfil);
             if (this.authservicio.usuarioLoguiado.perfil == undefined) {
               swal("Error", "Usuario no encontrado", "error");
             } else {
               this.router.navigate(['/principal']);
             }
 
           }, (err) => {
             swal("Error", "Usuario no encontrado", "error");
             this.spinner = false;
             console.log("usuario ni encontrado en base", err);
            
            });
         }, 1500);
       }, (err) => {
         swal("Error", "Usuario no encontrado en Auth", "error");
         this.spinner = false;
         console.log("usuario ni encontrado", err);
        
        });
 
      }
 
   }
 
 */





  public getOk(): boolean {
    return this.ok;
  }

  public getErrorDatos(): boolean {
    return this.errorDatos;
  }

  public getEnEspera(): boolean {
    return this.enEspera;
  }

  facebookLogin() {

    this.authservicio.facebookLogin().then((result) => {
      // This gives you a Facebook Access Token. You can use it to access the Facebook API.
      var token = result.credential.accessToken;
      //this.router.navigate(['/Principal']);
      // The signed-in user info.
      var user = result.user;
      console.info(token);
      this.fotoservice.buscarUsuarioPorEmail(result.user.email).subscribe(usuarios => {
        this.authservicio.usuarioLoguiado = usuarios[0];
        this.spinner = false;

        if (this.authservicio.usuarioLoguiado == undefined) {
          swal("Error", "Usuario no encontrado", "error");
        } else {
          console.log(this.authservicio.usuarioLoguiado.email);
          //this.fotoservice.actualizarDatos(this.authservicio.usuarioLoguiado.id, res);
          // swal("Usuario ingresado");
          this.router.navigate(['/Principal']);
        }
      })

    }).catch(function (error) {
      // Handle Errors here.
      console.info(error);
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
      // ...
    });


  }





  login() {

    if (this.formulario.valid) {

      this.spinner = true;

      this.usuario.email = this.formulario.value.correo;
      this.usuario.clave = this.formulario.value.clave;

      this.authservicio.usuarioLoguiado = new Usuario();
      this.authservicio.login(this.usuario).then((res) => {
        console.info(res);
        setTimeout(() => {
          this.fotoservice.buscarUsuarioPorEmail(this.usuario.email).subscribe(usuarios => {
            this.authservicio.usuarioLoguiado = usuarios[0];
            this.spinner = false;

            if (this.authservicio.usuarioLoguiado == undefined) {
              swal("Error", "Usuario no encontrado", "error");
            } else {
              console.log(this.authservicio.usuarioLoguiado.email);
              //this.fotoservice.actualizarDatos(this.authservicio.usuarioLoguiado.id, res);
              // swal("Usuario ingresado");
              this.router.navigate(['/Principal']);
            }
          }, (err) => {
            swal("Error", "Usuario no encontrado", "error");
            this.spinner = false;
            console.log("usuario ni encontrado en base", err);

          });
        }, 1500);
      }, (err) => {
        swal("Error", "Usuario no encontrado en Auth", "error");
        this.spinner = false;
        console.log("usuario ni encontrado", err);

      });

    }

  }


}
