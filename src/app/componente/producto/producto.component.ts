import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/clases/producto';
import { AuthService } from 'src/app/servicios/auth.service';
import { ProductoService } from 'src/app/servicios/producto.service';
import { Usuario } from 'src/app/clases/usuario';
import { FotoService } from 'src/app/servicios/foto.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
  public nuevoProducto: Producto;
  public archivo: File;
  files: File[] = [];

  usuario: Usuario;
  perfil: string;


 

  constructor(private serviciousuarios: FotoService,  public authservice: AuthService, public productoservice: ProductoService) { 
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil;
    console.log('perfil', this.perfil);
    this.nuevoProducto = new Producto();
  }

  ngOnInit() {
  }

 	onSelect($event) {
		console.log($event);
		this.files.push(...$event.addedFiles);
    this.archivo = this.files[0];
	}

	onRemove($event) {
		console.log($event);
		this.files.splice(this.files.indexOf($event), 1);
  }
  
  registrarProducto(){
    //lo agrego parar registro de login
    //this.authservice.SignUp(this.nuevoProducto);
    //lo agrego a la base y subo la fotos al storage
    this.productoservice.subir(this.archivo.name, this.archivo, this.nuevoProducto);
  }
  
 //var file = campoArchivo.get(0).files[0];
//subir(filename: string, file: any, perfil: string, usuario: Usuario) {}

}


