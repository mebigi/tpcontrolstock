import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/servicios/producto.service';
import { Producto } from 'src/app/clases/producto';
import { Router } from '@angular/router';
import { FotoService } from 'src/app/servicios/foto.service';
import { AuthService } from 'src/app/servicios/auth.service';
import { Usuario } from 'src/app/clases/usuario';
import { Pedido } from 'src/app/clases/pedido';
import { PedidoService } from 'src/app/servicios/pedido.service';
import { EstadoPedido } from 'src/app/enumerados/estado-pedido.enum';


@Component({
  selector: 'app-productos-listado',
  templateUrl: './productos-listado.component.html',
  styleUrls: ['./productos-listado.component.css']
})
export class ProductosListadoComponent implements OnInit {
  productos: Producto[] = [];
  filtro: Producto[] = [];
  usuario: Usuario;
  perfil: string;
  pedido: Pedido;
  productosPedidos: Producto[] = [];
  mesa:string = "";
  costoTotal: number =0;



  constructor(private serviciousuarios: FotoService, private authservice: AuthService, private servicioproductos: ProductoService, private pedidoServicio: PedidoService, private route: Router) {
  
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil;
    console.log('perfil', this.perfil);
  }

  ngOnInit() {
    this.traerTodasUsuarios();
    // this.prueba();
  
  
  }

getUnique(arr, comp) {
    var unique = arr
        .map(function (e) { return e[comp]; })
        // store the keys of the unique objects
        .map(function (e, i, final) { return final.indexOf(e) === i && i; })
        // eliminate the dead keys & store unique objects
        .filter(function (e) { return arr[e]; }).map(function (e) { return arr[e]; });
    return unique;
}



  traerTodasUsuarios() {
    this.servicioproductos.traerTodos('productos')
      .subscribe(productos => {
        this.productos = productos;
        // console.log(this.usuarios[0].numero);
        this.filtro = this.getUnique(this.productos, 'sucursal');
        console.info(this.filtro);
      });
  }


  prueba() {
    this.servicioproductos.prueba();
  }


  mostrar(producto: any) {
    //this.paisesService.verDetalle(pais).subscribe(resp => resp);
    this.servicioproductos.verDetalle(producto).subscribe(resp => resp);
  }

  agregar(producto: any) {
    //this.paisesService.verDetalle(pais).subscribe(resp => resp);
    let productoPedido = new Producto();
    productoPedido.URL = producto.URL;
    productoPedido.categoria = producto.categoria;
    productoPedido.codigo = producto.codigo;
    productoPedido.costo = producto.costo;
    productoPedido.descripcion = producto.descripcion;
    productoPedido.nombre = producto.nombre;
    productoPedido.tipo_empledo = producto.tipo_empledo;
    productoPedido.cantidad = producto.cantidad;
    this.productosPedidos.push(productoPedido);
    this.costoTotal = this.costoTotal + (productoPedido.costo*productoPedido.cantidad);
  }


  quitarProducto(producto: any){
    const index = this.productosPedidos.indexOf(producto);
    this.productosPedidos.splice(index, 1);
    this.costoTotal = this.costoTotal - (producto.costo*producto.cantidad);

  }


  IniciarPedido(){
    this.pedido=new Pedido();
    this.mesa = "1";
    this.pedido.cliente.nombre = "Pepe";
    this.pedido.mesas.push(this.mesa);//ocupar mesa
    this.pedido.mozo = this.usuario;//mozo
    this.pedido.estado = EstadoPedido.iniciado;
    //nombre cliente   
  }


  RealizarPedido(){
     
    if( this.pedido !=undefined && this.pedido.estado == "iniciado" && this.productosPedidos.length > 0){
      this.pedido.productos = this.productosPedidos;
      this.pedido.estado= EstadoPedido.pendiente;
      this.pedidoServicio.subir(this.pedido);// the la respuesta si es ok habilito this.pedido = new pedido()
    }
    else {
     console.log("el pedido no fue iniciado o no tiene productos seleccionados");
    }
  
  }

}
