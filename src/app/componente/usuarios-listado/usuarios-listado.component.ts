import { Component, OnInit } from '@angular/core';
import { FotoService } from 'src/app/servicios/foto.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/servicios/auth.service';
import { Usuario } from 'src/app/clases/usuario';

@Component({
  selector: 'app-usuarios-listado',
  templateUrl: './usuarios-listado.component.html',
  styleUrls: ['./usuarios-listado.component.css']
})
export class UsuariosListadoComponent implements OnInit {
  usuarios: Usuario[];
  usuario: Usuario;
  perfil: string;

  constructor(private authservice: AuthService, private serviciousuarios: FotoService, private route:Router) { 
    this.usuario = new Usuario;
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil;
    
  }

  ngOnInit() {
    this.traerTodasUsuarios();
  }

  traerTodasUsuarios() {
    this.serviciousuarios.traerTodos()
      .subscribe(usuarios => {
        this.usuarios = usuarios;
        // console.log(this.usuarios[0].numero);
      });
  }


  mostrar(usuario: Usuario){
    console.log('usuario detelle', usuario.id);
       //this.paisesService.verDetalle(pais).subscribe(resp => resp);
    this.serviciousuarios.verDetalle(usuario).subscribe(resp => this.route.navigate(['Usuario-info']));
  }

}
