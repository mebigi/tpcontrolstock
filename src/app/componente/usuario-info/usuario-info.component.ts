import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/clases/usuario';
import { AuthService } from 'src/app/servicios/auth.service';
import { ProductoService } from 'src/app/servicios/producto.service';
import { FotoService } from 'src/app/servicios/foto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-usuario-info',
  templateUrl: './usuario-info.component.html',
  styleUrls: ['./usuario-info.component.css']
})
export class UsuarioInfoComponent implements OnInit {
  usuario: Usuario;
  perfil: string;
  movimientos: any[];
  usuarioSelec: any;
  locales: any[];

  constructor(private authservice: AuthService, private servicioproductos: ProductoService, private serviciousuarios: FotoService, private route:Router) { 
    this.usuario = this.authservice.usuarioLoguiado;
    this.perfil = this.authservice.usuarioLoguiado.perfil; 
    this.usuarioSelec = this.serviciousuarios.usuarioSelecinado;
   
  }


  ngOnInit() {
    this.traerTodaMovimientos();
    this.traerTodos();
    
  }


  traerTodos() {
    this.servicioproductos.traerTodos('locales')
       .subscribe(productos => {
         this.locales = productos;
         // console.log(this.usuarios[0].numero);
       });
  }
   
  traerTodaMovimientos() {
    this.serviciousuarios.traerTodosMovimientos(this.usuarioSelec.id, 'usuarios')
      .subscribe(movimientos => {
        this.movimientos = movimientos;
      });
  }

  cambiar(){
    this.movimientos=[];
    this.serviciousuarios.cambiar(this.usuarioSelec.id, this.usuarioSelec);
  }
  

}
